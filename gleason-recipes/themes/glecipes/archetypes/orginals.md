---
title: "{{ replace .Name "-" " " | title }}"
date: {{ .Date }}
draft: false
---

# {{ replace .Name "-" " " | title }}

{{< directoryindex path="/static/orginal-recipes/{{.Name}}" pathURL="/orginal-recipes/{{.Name}}" >}}