---
title: "Orginals"
date: 2022-12-29T22:16:57-05:00
draft: false

---
# Orginals
- ### [Appetizers](./appetizers/)
- ### [Beverages](./beverages/)
- ### [Bread](./breads/)
- ### [Breakfast](./breakfast/)
- ### [Canning-Preserving](./canning-preserving/)
- ### [Desserts](./desserts/)
- ### [Main Dishes](./main_dishes/)
- ### [Miscellaneous](./miscellaneous/)
- ### [Salads](./salads/)
- ### [Sandwiches](./sandwiches/)
- ### [Soups](./soups/)
- ### [Vegetables](./vegetables/)



