---
title: "Almond Lace Wafers"
date: 2023-12-23T19:38:15-05:00
draft: false
description: "Crunchy and Delicate Almond Ice Cream Topper"
tags: ['dessert','christmas',]
categories: ['orginals']
recipes: 
  name: "Almond Lace Wafers"
  author:
    name: "Grandma Snow"
    link: "https://recipewebsite.com"
  prepTime: "25"
  cookTime: "35"
  totalTime: "60"
  yield: "10"
  ingredients: [
      [
        name: "Almond Wafers",
        ingredient: [
            "1 1/2 cups finely ground almonds",
            "1 cup flour",
            "1 cup sugar",
            "2 tbps flour",
            "2 tbps heavy cream",
            "2 tbps milk",
          ],
      ],
      [
        name: "Ice Cream Pie",
        ingredient: [
          "2 qt (half a gallon) Ice Cream (Pepermint Ice Cream)",
          "2 Jars (16 oz each) Hot Fudge",
        ],
      ]

  ]
  instructions: [
    [
    name: "Combine Ingredients",
    text: "(Pre-Heat Ovean to 350 degrees) - Combine All Ingredients thorughly mix in large bowl."
    ],
    [
      "name": "Cook and add to sheet",
          "text": "Cook in heavy pan over low heat until better melts. In a well greased and floured baking sheet, add a heaping spoonsful 3 inches apart."
    ],
    [
          "name": "Bake",
          "text": "Bake at 350 degrees for about 6 to 12 minutes or until slightly brown and still bubbling in the center. "
     ],
     [ 
          "name": "Cool and Roll",
          "text": "Let Cool only until edges is firm enough to lift with a spatula, then working working quickly lift cookies onto parchment paper turning top-side down. Roll over handle of wooden sppon. (If making Ice Cream pie leave ~2/3 wafers flat.) Re-grease cookie sheet each time you bake"
     ],
     [
          "name": "Making the Almond Wafers Ice Cream Pie",
          "text": "In a large square glass cassarole dish, make laters of the follow. Bottom to Top -  Flat Wafers, Ice cream (cut into 1/2 - 1 inch), Hot Fudge, Flat Wafers, Ice Cream, Hot Fudge, Topped with crumbles of wafers. - Freeze until Ice Cream is hard ~4 hours to overnight  "
     ],
]
  ProTips: ["Use Food Processor or Slap Chop to finely chop almonds.", "Once wafers turn brown, watch closesly (add only 30 seconds at a time) - Brown and turn to burnt fast", "If you over pour / wafers touch when comming out of the oven, you can still seaprate them while they are hot, before the cool", "To help create a layers of ice cream either cut the forzen ice cream (paper) container, or place icream container in hot water and plop icream onto plate to cut into layers"]
  images: ['../Almond-IceCream-Dessert.jpeg']
---

