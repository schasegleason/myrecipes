---
title: "Banana Muffins"
date: 2023-02-21T13:21:11-05:00
draft: false
description: "Yummy Muffins"
tags: ['breakfast','baking',]
categories: ['new finds', 'chase']
recipes: 
  name: "Banana Muffins"
  author:
    name: "Abi Godfrey / All Recipes"
    link: "https://www.allrecipes.com/recipe/42719/banana-muffins-ii/"
  prepTime: "10"
  cookTime: "30"
  totalTime: "40"
  yield: "12"
  ingredients: [
      [
        name: "Main",
        ingredient: [
            "1 1/2 cups flour",
            "1 teaspoon baking powder",
            "1 teaspoon baking soad",
            "1/2 teaspoons salt",
            "3/4 cup sugar",
            "1 egg",
            "3 large very ripe Bananas, mashed",
            "1/3 cup butter, melted",
            "6 oz Semi Sweet Chocolate Chips (Optional)",
          ],
      ],

  ]
  instructions: [
    [
    name: "Prep Oven and Muffin Tin",
    text: "Preheat Oven to 350 degrees F. Pam / Grease a 12-cup muffin tin."
    ],
    [
      "name": "Mix dry ingredients",
          "text": "Mix flour, baking powder, baking soad, and salt together in a small bowl."
    ],
    [
          "name": "Mix wet ingredients",
          "text": "In a large bowl add, banans, sugar, eggs and melted butter. Mash and Mix until smooth."
     ],
     [ 
          "name": "Combine wet and dry ingredients",
          "text": "Add the dry ingredients to the wet ingredients, combine / stir until well combined / a batter has formed. Add any choolcate chips at this point."
     ],
     [
          "name": "Bake",
          "text": "Spoon the batter into the Muffin tin, filling each muffin up about 2/3 full. Bake for 25-30 minutes."
     ],

]
  ProTips: [""]
  images: [""]
---

