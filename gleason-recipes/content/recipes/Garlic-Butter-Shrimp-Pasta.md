---
title: "Garlic Butter Shrimp Pasta"
date: 2022-02-10T22:47:04-05:00
draft: false
description: "Garlic Butter Shrimp Pasta"
tags: ['dinner','italian','seafood']
categories: ['new finds','chase']
recipes: 
  name: "Garlic Butter Shrimp Pasta"
  author:
    name: "Chungah"
    link: "https://damndelicious.net/2015/03/13/garlic-butter-shrimp-pasta/"
  prepTime: "15"
  cookTime: "10"
  totalTime: "25"
  yield: "4"
  ingredients: [
      [
        name: "Main",
        ingredient: [
            "8 ounces (1/2 Box) fettuccine or any long pasta",
            "1 pound medium/large shrimp, peeled and deveined",
            "Kosher salt and freshly ground black pepper, to taste",
            "8 tablespoons (1 stick) unsalted butter, divided",
            "4 cloves garlic, minced",
            "1/2 teaspoon dried oregano (Sub w/ Thyme or Basil)",
            "1/2 teaspoon crushed red pepper flakes (Sub w/ Chilli poweder)",
            "1/4 cup freshly grated Parmesan",
            "2 tablespoons parsley",
          ],
      ],

  ]
  instructions: [
    [
    name: "Cook Pasta",
    text: "In a large pot of boiling salted water, cook pasta (al dente) according to package instructions; drain well."
    ],
    [
      "name": "Prep Shrimp",
          "text": "Ensure Shrimp is peeled and deveined. Season shrimp with salt and pepper, to taste; set aside."
    ],
    [
          "name": "Melt Butter",
          "text": "Melt 2 tablespoons butter in a large skillet over medium high heat. Add garlic, oregano and red pepper flakes, and cook, stirring frequently, until fragrant, about 1-2 minutes. "
     ],
     [ 
          "name": "Cook Shrimp",
          "text": "Add shrimp and cook, stirring occasionally, until pink, about 2-3 minutes; set aside."
     ],
     [
          "name": "Stir in Pasta",
          "text": "Melt remaining 6 tablespoons butter in the skillet. Stir in pasta, arugula and Parmesan until arugula begins to wilt, about 2 minutes. Stir in shrimp."
     ],
      [
          "name": "Serve",
          "text": "Serve immediately, garnished with parsley, if desired.",
      ],
]
  ProTips: []
  images: []
---

