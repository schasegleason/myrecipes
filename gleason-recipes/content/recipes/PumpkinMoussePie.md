---
title: "Pumpkin Mousse Pie"
date: 2023-10-22T19:19:55-04:00
draft: false
description: "Pumpkin Mousse Pie"
tags: ['dessert',]
categories: ['orginals']
recipes: 
  name: "PumpkinMoussePie"
  author:
    name: "Crazy for Crust"
    link: "https://www.crazyforcrust.com/no-bake-pumpkin-pie/#wprm-recipe-container-43696"
  prepTime: "15"
  cookTime: "60"
  totalTime: "75"
  yield: "6"
  ingredients: [
      [
        name: "Main",
        ingredient: [
            "1 can (15 oz) pumpkin puree",
            "1 box (3.4 ounce) instant vanilla pudding",
            "¼ cup milk or cream",
            "2 teaspoons pumpkin pie spice",
            "1 container (8 oz) Cool Whips",
            "1 (9-inch) graham cracker crust (store-bought or homemade)",
            "Whipped Cream for serving",
          ],
      ],

  ]
  instructions: [
    [
    name: "Combine Ingredients",
    text: "Stir together pumpkin puree, pudding mix, milk, and pumpkin pie spice. Stir until no lumps remain."
    ],
    [
      "name": "Fold Cool Whip",
          "text": "Carefully fold in Cool Whip."
    ],
    [
          "name": "Add to Crust and Chill",
          "text": "Spread pumpkin mixture in prepared crust. Chill at least 3 hours or until it sets enough to slice. Serve with whipped cream."
     ],
]
  ProTips: []
  images: []
---

