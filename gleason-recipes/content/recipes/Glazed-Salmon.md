---
title: "Glazed Salmon"
date: 2022-02-10T22:23:22-05:00
draft: false
description: "Honey-Soy Glazed Salmon"
tags: ['dinner','asian', 'seafood']
categories: ['new finds','chase']
recipes: 
  name: "Glazed Salmon"
  author:
    name: "Blake Royer"
    link: "https://www.seriouseats.com/dinner-tonight-broiled-honey-soy-teriyaki-salmon-with-boy-choy-recipe"
  prepTime: "20"
  cookTime: "10"
  totalTime: "35"
  yield: "2-4"
  ingredients: [
      [
        name: "Main",
        ingredient: [
          "1 cup soy sauce",
          "1/4 cup honey",
          "2 tablespoons lemon juice",
          "1 tablespoon ground ginger",
          "2-4 salmon fillets",
          ],
      ],
    

  ]
  instructions: [
    [
    name: "Mix Marinade",
    text: "Whisk soy sauce, honey, lemon juice, ginger, and garlic together until honey dissolves. Reserve 1/4 cup of marinade in separate bowl, then place salmon fillets, skin-side up, in marinade in a bag. Allow to marinate for at least 10 minutes, preferably for 30 but 20 minutes works fine."
    ],
    [
      "name": "Prep Sides",
          "text": "Nuke Forzen Veggies and/or make rice"
    ],
    [
          "name": "Broil Salmon",
          "text": "Put the salmon under the broiler skin-side down and broil without turning until exterior is well-caramelized and the fish is just cooked through, 7-10 minutes, depending on thickness and the distance from broiler. While cooking take reversed marinade and reduce in small sauce pan or glass dish. And Serve"
     ],
    
]
  ProTips: ["Don't get / add extra marinade on the baking sheet you are broiling with, it will burn an smell", "Use foil over baking sheet for easy cleanup"]
  images: []
---

