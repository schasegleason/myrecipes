---
title: "Puff Pancakes"
date: 2022-12-10T10:54:54-05:00
draft: false
description: "Puff Pancakes"
tags: ['breakfast']
categories: ['orginals']
recipes: 
  name: "Puff Pancakes"
  author:
    name: "Grandma Snow"
  prepTime: "15"
  cookTime: "15"
  totalTime: "30"
  yield: "6-10"
  ingredients: [
      [
        name: "Main",
        ingredient: [
            "2 eggs",
            "1 cup mulk",
            "2 - 1/3 cups bisquick",
            "2 tablespoon sugar",
            "1/4 cup melted butter or vegetable oil",
          ],
      ]
  ]
  instructions: [
    [
    name: "Beat Eggs, Blend Milk",
    text: "Beat Eggs until softish peaks form, (or until foamy for less puff.) Afterwards, Slowly Blend in Milk."
    ],
    [
      "name": "Add and Combine Dry ingredients",
          "text": "Add Bisquick, and Sugar. Mix until throughly dampended. Blend in melted butter or oil."
    ],
    [
          "name": "Bake",
          "text": "Spoon onto hot griddle, or non-stick pan (~1/3 Cup per pancake). Make sure to use Pam."
     ],
   
]
  ProTips: ["The amount of puff in the pancake depends on the peaks of the eggs"]
  images: ["../Puff_Pancake.PNG"]
---

