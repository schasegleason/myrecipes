---
title: "DinnerRolls"
date: 2021-11-27T21:21:54-05:00
draft: false
description: "Perfect Homemade Dinner Rolls Light and fluffy"
tags: ['dinner','christmas','thanksgiving']
categories: ['orginals']
recipes: 
  name: "Homemade Dinner Rolls"
  author:
    name: "Grandma Snow"
  prepTime: "30"
  cookTime: "15"
  totalTime: "180"
  yield: "10"
  ingredients: [
    [
      name: "Main",
      ingredient: [
        "2 (7g - Packets) Active Dry Yeast",
        "2/3 cup warm water",
        "1 teaspoons sugar",
        "1/2 cup butter",
        "1 cup warm water",
        "3 eggs, well beaten",
        "1/2 cup sugar",
        "1 teaspoon salt",
        "4 & 1/2 cups flour",
      ]
    ]
  ]
  instructions: [
    [
    name: "Prep Yeast",
    text: "(Follow Instructions of Yeast Packet) - Dissolve Yeast in 1/2 cup warm water to which 1 teaspoon sugar has been added. Set Aside."
    ],
    [
      "name": "Mix wet ingredients",
          "text": "Melt Butter in 1 cup water over medium heat. Add well Beaten eggs. Add Sugar, Sald and water-butter mixture. If nescessary, cool (to not kill yeast)"
    ],
    [
          "name": "Mix wet and dry ingredients",
          "text": "Add yeast mixture and 2 cups flour. Beat well (with wooden spoon, electric beaters, or dough hook. Beat in remaining flour. Dough will be very sticky. "
     ],
     [ 
          "name": "Allow to Rise",
          "text": "Cover the flour and yeast mixture and allow to rise unit double in size / bulk (approximately 1 hour.) Stir / punch down cover tightly. Place in refriggerator for several horus or overnight."
     ],
     [
          "name": "Prep for Baking",
          "text": "Two hours before baking. Set out several baking sheets amd cover / smoother in button (best to add butter to sheet and stick  a warm oven to melt). Remove dough from refrigerator and turn out onto floured surface. (Add flour as needed to prevent sticking) Cut Dought into three parts. Roll each part into 10-ich circles. Use large cup or tool to cut dough into 1.5 - 2 inch circles. Add a tab of butter into the middle of each circle. Fold the dough circles in half pinching the edges close. Add to butter cover baking sheets rolling / covering each roll in butter. Cover lightly / with kitchen towel, and allow to rise / double - approixmatly two hours."
     ],
      [
          "name": "Baking",
          "text": "Bake at 400f for 10 - 15 minutes (until very slighly browned on top) - Makes 3 dozen rolls",
      ],
]
  ProTips: ["ProTip1", "proTip2"]
  images: ["dinnerRollsSecondProof.jpg"]
  
---