---
title: "Carnitas"
date: 2024-02-26T00:00:00-05:00
draft: false
description: "Juicy, tender shredded pork with crispy, flavorful edges, made effortlessly in a slow cooker."
tags: ['dinner', 'Mexican', 'crockpot']
categories: ['Chase']
recipes:
  name: "Carnitas (Slow Cooker Pork Carnitas)"
  author:
    name: "Lisa Bryan"
    link: "https://downshiftology.com/recipes/carnitas/"
  prepTime: "10"
  cookTime: "480"
  totalTime: "490"
  yield: "8 servings"
  ingredients: [
    [
      name: "Main Ingredients",
      ingredient: [
        "4 pounds boneless pork shoulder or butt",
        "1 tablespoon dried oregano",
        "1 tablespoon ground cumin",
        "2 teaspoons chili powder",
        "1 teaspoon salt",
        "½ teaspoon black pepper",
        "1 yellow onion, diced",
        "4 garlic cloves, minced",
        "1 jalapeño pepper, deseeded and finely diced",
        "Juice of 1 orange"
      ],
    ],
  ]
  instructions: [
    [
      name: "Prepare the Pork",
      text: "Trim any excess fat from the pork shoulder, leaving some for flavor. In a small bowl, mix together the oregano, cumin, chili powder, salt, and black pepper. Rub this spice mixture generously over the entire pork shoulder."
    ],
    [
      name: "Slow Cook",
      text: "Place the seasoned pork in the slow cooker. Add the diced onion, minced garlic, jalapeño, and orange juice. Cover and cook on low for 8 to 10 hours, or on high for 5 to 6 hours, until the meat is tender and falls apart easily."
    ],
    [
      name: "Shred the Pork",
      text: "Remove the pork from the slow cooker and place it on a large cutting board. Use two forks to shred the meat. Reserve the cooking liquid."
    ],
    [
      name: "Optional: Crisp the Pork",
      text: "For crispy edges, spread the shredded pork in an even layer on a baking sheet. Pour 1 cup of the reserved cooking liquid over the meat. Broil in the oven for 5 to 10 minutes, or until the edges are golden and crispy."
    ],
    [
      name: "Serve",
      text: "Serve the carnitas warm in tacos, burritos, or over rice, garnished with your favorite toppings."
    ],
  ]
  ProTips: [
    "For added flavor, marinate the pork with the spice rub overnight in the refrigerator before cooking.",
    "Leftover carnitas can be stored in an airtight container in the refrigerator for up to 4 days or frozen for up to 3 months."
  ]
  images: [""]
---
