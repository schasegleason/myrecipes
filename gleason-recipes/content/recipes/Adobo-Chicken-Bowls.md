---
title: "Adobo Chipotle Chicken Bowls"
date: 2023-11-27T12:16:35-05:00
draft: false
description: "A flavorful combination of sweet, spicy, and smoky marinated chicken served with corn salsa and avocado over rice."
tags: ['dinner', 'mexican']
categories: ['Chase']
recipes:
  name: "Adobo Chipotle Chicken Bowls"
  author:
    name: "Marzia / Little Spice Jar"
    link: "https://littlespicejar.com/honey-chipotle-chicken-bowls/"
  prepTime: "15"
  cookTime: "20"
  totalTime: "1 hour 5 minutes"
  yield: "4-5 servings"
  ingredients: [
    [
      name: "Chicken and Marinade",
      ingredient: [
        "1½ pounds boneless, skinless chicken (thighs, breasts, or tenders)",
        "1-2 chipotle peppers in adobo sauce (plus 1 tbsp adobo sauce)",
        "1½ tablespoons red wine vinegar",
        "1 tablespoon honey",
        "3 tablespoons olive oil",
        "2 small garlic cloves, minced",
        "½ teaspoon onion powder",
        "½ teaspoon dried oregano",
        "1 teaspoon salt",
        "¼ teaspoon black pepper"
      ],
    ],
    [
      name: "Corn Salsa",
      ingredient: [
        "1 (16-ounce) bag frozen corn",
        "⅓ cup red onion, finely chopped",
        "1 large poblano pepper, charred, peeled, and finely chopped",
        "1 jalapeño pepper, finely minced",
        "¼ cup cilantro, chopped",
        "1 tablespoon lemon juice",
        "1 tablespoon lime juice",
        "Salt, to taste"
      ],
    ],
    [
      name: "For Serving",
      ingredient: [
        "2½ cups prepared rice, quinoa, or grains",
        "Lime wedges",
        "Cilantro",
        "Sliced avocados"
      ],
    ],
  ]
  instructions: [
    [
      name: "Prepare Marinade",
      text: "Mince the chipotle peppers and combine with adobo sauce, red wine vinegar, honey, olive oil, minced garlic, onion powder, dried oregano, salt, and black pepper in a jar. Shake or whisk to combine."
    ],
    [
      name: "Marinate Chicken",
      text: "Place the chicken in a dish and pour the marinade over it. Allow it to marinate for at least 30 minutes, ideally up to 4 hours."
    ],
    [
      name: "Char Poblano Pepper",
      text: "Char the poblano pepper over an open flame or under the broiler until the skin is blistered. Place in a bowl, cover, and let sit for 10 minutes. Peel off the skin and finely chop."
    ],
    [
      name: "Prepare Corn Salsa",
      text: "Microwave the frozen corn until defrosted and slightly warm. In a bowl, combine corn, red onion, chopped poblano, minced jalapeño, cilantro, lemon juice, lime juice, and a pinch of salt. Stir to combine and refrigerate until ready to serve."
    ],
    [
      name: "Cook Chicken",
      text: "Heat a grill pan over medium-high heat. Cook the marinated chicken for 8-10 minutes, flipping halfway, until it reaches an internal temperature of 160°F. Remove from heat and let rest for 10 minutes before slicing or dicing."
    ],
    [
      name: "Assemble Bowls",
      text: "In serving bowls, add a base of prepared rice or grains. Top with sliced chicken, a scoop of corn salsa, and sliced avocados. Garnish with lime wedges and cilantro."
    ],
  ]
  ProTips: [
    "For added flavor, char the corn before adding it to the salsa.",
    "Adjust the number of chipotle peppers based on your heat preference."
  ]
  images: [""]
---
