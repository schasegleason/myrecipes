---
title: "PoppySeedBread"
date: 2021-12-12T15:46:41-05:00
draft: false
description: "Christmas Poppy Seed Bread"
tags: ['breakfast','dessert','christmas']
categories: ['orginals']
recipes: 
  name: "PoppySeedBread"
  author:
    name: "Some cook book"
  prepTime: "20"
  cookTime: "60"
  totalTime: "90"
  yield: "2 loaves"
  ingredients: [
      [
        name: "Main",
        ingredient: [
            "3 cups flour",
            "2 1/2 cups sugar",
            "1 1/2 teaspoons salt",
            "1 1/2 teaspoons baking powder",
            "3 eggs",
            "1 1/2 cups milk",
            "1 1/8 cups vegetable oil",
            "1 1/2 teaspoon poppy seeds",
            "1 1/2 teaspoon vanilla extract",
            "1 1/2 teaspoon almond extract",
            "1 1/2 teaspoon butter extract/flavoring",
          ],
      ],
      [
        name: "Glaze",
        ingredient: [
          "3/4 cup sugar",
          "1/4 cup orange juice",
          "1/2 teaspoon vanilla extract",
          "1/2 teaspoon almond extract",
          "1/2 teaspoon butter extract/flavoring",
        ],
      ]

  ]
  instructions: [
    [
    name: "Prep",
    text: "Grease / Pam two 9 x 5 or three smaller tins/pans. Preheat oven to 350"
    ],
    [
      "name": "Mix main ingredients",
          "text": "Mix all main bread ingredients together, then beat for 2 minutes."
    ],
    [
          "name": "Pour and Bake",
          "text": "Pour batter into pans and bake for ~1 hour, until baked through, check with tooth pick, if needed"
     ],
     [ 
          "name": "Mix glaze ingredients",
          "text": "Mix glaze ingredients togehter into small bowl and pour over hot bread in pans. Let cool before removing / cutting"
     ],
     
]
  ProTips: ["Use the smaller disposal tin pans for great gifts", ""]
  images: ["../poppyseedbread.jpeg"]
---

