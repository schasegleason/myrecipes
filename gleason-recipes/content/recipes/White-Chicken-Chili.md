---
title: "White Chicken Chili"
date: 2023-03-16T00:00:00-05:00
draft: false
description: "A healthy and hearty slow cooker white chicken chili made with salsa verde, perfect for meal prepping."
tags: ['dinner', 'crockpot']
categories: ['Chase']
recipes:
  name: "White Chicken Chili with Salsa Verde"
  author:
    name: "Elysia Cartlidge"
    link: "https://www.hauteandhealthyliving.com/white-chicken-chili/"
  prepTime: "10"
  cookTime: "360"
  totalTime: "370"
  yield: "6 servings"
  ingredients: [
    [
      name: "Main Ingredients",
      ingredient: [
        "1 pound boneless, skinless chicken breasts",
        "2 (19-ounce) cans no salt-added cannellini beans, rinsed and drained",
        "1 cup salsa verde",
        "1 (4-ounce) can mild green chiles",
        "2 teaspoons ground cumin",
        "1 cup frozen corn",
        "3-4 cups reduced-sodium chicken broth",
        "Salt and black pepper, to taste"
      ],
    ],
    [
      name: "Optional Toppings",
      ingredient: [
        "Fresh lime juice",
        "Chopped cilantro",
        "Sliced avocado",
        "Tortilla strips or pita chips"
      ],
    ],
  ]
  instructions: [
    [
      name: "Assemble Ingredients in Slow Cooker",
      text: "Place chicken breasts in the bottom of a 6-quart or larger slow cooker. Top with cannellini beans, salsa verde, green chiles, cumin, and 1 cup of corn. Add 3 cups of chicken broth and stir to combine."
    ],
    [
      name: "Cook",
      text: "Cover and cook on low for 6-8 hours or on high for 3-4 hours, until the chicken is cooked through and tender."
    ],
    [
      name: "Shred Chicken",
      text: "Remove the chicken breasts from the slow cooker and place them on a plate. Once cool enough to handle, shred the chicken using two forks."
    ],
    [
      name: "Thicken Chili",
      text: "Using an immersion blender, puree a portion of the chili in the slow cooker to thicken it, leaving some beans whole. Alternatively, transfer a few ladles of the chili to a food processor, blend, and return to the slow cooker. Adjust the consistency by adding more chicken broth if desired."
    ],
    [
      name: "Combine and Season",
      text: "Stir the shredded chicken back into the slow cooker. Season with salt and black pepper to taste. Cook for an additional 30 minutes to allow the chili to thicken."
    ],
    [
      name: "Serve",
      text: "Portion the white chicken chili into bowls. Top with a squeeze of fresh lime juice and garnish with desired toppings such as chopped cilantro, sliced avocado, and tortilla strips or pita chips."
    ],
  ]
  ProTips: [
    "For a thicker chili, blend a portion of the soup using an immersion blender or regular blender.",
    "To add extra texture and color, stir in additional corn after pureeing the soup.",
    "If using rotisserie chicken, add the shredded chicken during the last few minutes of cooking."
  ]
  images: [""]
---
