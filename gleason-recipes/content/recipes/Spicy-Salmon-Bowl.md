---
title: "Spicy Salmon Bowl"
date: 2024-11-27T13:33:51-05:00
draft: false
description: "A quick and easy spicy salmon bowl that's gluten-free, dairy-free, and customizable with your favorite toppings."
tags: ['dinner', 'seafood']
categories: ['Chase']
recipes:
  name: "Spicy Salmon Bowl"
  author:
    name: "Bailey"
    link: "https://www.wholekitchensink.com/spicy-salmon-bowl/"
  prepTime: "20"
  cookTime: "10"
  totalTime: "30"
  yield: "2 servings"
  ingredients: [
    [
      name: "Spicy Salmon",
      ingredient: [
        "1 pound salmon, skin removed and cut into 1-inch pieces",
        "¼ cup rice vinegar",
        "¼ cup soy sauce or coconut aminos",
        "1 tablespoon sesame oil",
        "1 tablespoon sriracha (or more to taste)",
        "1 tablespoon honey",
        "½ teaspoon garlic powder or 1 teaspoon minced garlic",
        "½ teaspoon onion powder"
      ],
    ],
    [
      name: "Spicy Mayo Sauce",
      ingredient: [
        "2 tablespoons mayo",
        "2 tablespoons sriracha",
        "2 tablespoons sweet chili sauce"
      ],
    ],
    [
      name: "Slaw",
      ingredient: [
        "4 cups shredded green cabbage",
        "2 tablespoons rice vinegar (or white wine vinegar)",
        "2 tablespoons soy sauce or coconut aminos",
        "2 teaspoons sesame oil",
        "2 teaspoons minced garlic",
        "2 teaspoons grated ginger",
        "2 green onions, chopped",
        "Pinch of salt and pepper"
      ],
    ],
  ]
  instructions: [
    [
      name: "Marinate Salmon",
      text: "In a bowl, combine rice vinegar, soy sauce, sesame oil, sriracha, honey, garlic powder, and onion powder. Add salmon pieces and toss to coat. Refrigerate for about 30 minutes."
    ],
    [
      name: "Prepare Spicy Mayo",
      text: "In a small bowl, mix mayo, sriracha, and sweet chili sauce until well combined. Refrigerate until ready to use."
    ],
    [
      name: "Prepare Slaw",
      text: "In a large bowl, combine shredded cabbage, rice vinegar, soy sauce, sesame oil, minced garlic, grated ginger, chopped green onions, salt, and pepper. Toss to mix and set aside."
    ],
    [
      name: "Cook Salmon",
      text: "Preheat oven to 400°F (200°C). Place marinated salmon pieces on a baking sheet lined with parchment paper. Bake for 10-12 minutes until cooked through."
    ],
    [
      name: "Assemble Bowls",
      text: "In serving bowls, add a base of cooked rice or grains. Top with baked salmon, slaw, and drizzle with spicy mayo. Add additional toppings as desired."
    ],
  ]
  ProTips: [
    "Customize your bowl with toppings like sliced cucumbers, avocado, sesame seeds, or nori strips.",
    "For a grain-free option, use cauliflower rice instead of regular rice."
  ]
  images: [""]
---
