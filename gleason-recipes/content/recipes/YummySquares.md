---
title: "YummySquares"
date: 2022-11-24T11:13:03-05:00
draft: false
description: "Yummy Sqaure Graham Cracker Brownies "
tags: ['dessert']
categories: ['orginals']
recipes: 
  name: "YummySquares"
  author:
    name: "Grandma Snow"
  prepTime: "15"
  cookTime: "30"
  totalTime: "45"
  yield: "10"
  ingredients: [
      [
        name: "Main",
        ingredient: [
            "1.5 Cups / 24 Graham Crackers (Crushed)",
            "6 oz (1/2 Bag) Chocolate Chips",
            "1 teaspoons Vanilla",
            "1/2 teaspoons Cinnamon",
            "1 Can (14 oz) Sweet and Condensed Milk",
          ],
      ],

  ]
  instructions: [
    [
    name: "Combine Ingredients",
    text: "PreHeat Oven to 350 - Combine all Ingredients and Mix Well"
    ],
    [
      "name": "Bake",
          "text": Pour into butter 8 x 11 Pan and Bake at 350 for 30 minutes (40-45 Minutes if Doubled)"
    ],

]
  ProTips: ["Buy PreCrushed Graham Crackers Crumbs, Typically near pie crusts"]
  images: ["dinnerRollsSecondProof.jpg"]
---

