---
title: "CaramelPopcorn"
date: 2021-12-12T16:31:07-05:00
draft: false
description: "Addictive Caramel Corn"
tags: ['dessert','christmas',]
categories: ['orginals']
recipes: 
  name: "CaramelPopcorn"
  author:
    name: "Grandma Snow"
  prepTime: "30"
  cookTime: "15"
  totalTime: "60"
  yield: "2 large containers"
  ingredients: [
      [
        name: "Main",
        ingredient: [
            "2 cups brown sugar",
            "1/2 lb. Butter",
            "1 teaspoons salt",
            "1/2 cup Karo/Ligh Corn syrup",
            "1 teaspoon vanilla extract",
            "1 teaspoon baking soad",
            "2 Cups PopCorn kernels"
          ],
      ],
 
  ]
  instructions: [
    [
      "name": "Prep",
      "text": "PreHeat Oven to 200. Pop 2 cups of popcorn kernels. Place popped popcorn onto two lightly buttered/Pamed cooking sheets"
    ],
    [
    name: "Melt and Mix",
    text: "In a Large pot, on medium/low heat, melt butter and mix together, Sugar, Salt, and Karo. One melted and mixed, bring to a boil then turn down and cook for four minutes"
    ],
    [
          "name": "Magic and Pour",
          "text": "Add vinilla and baking soad, stir well and pour over popcorn in baking sheets. "
     ],
     [ 
          "name": "Bake",
          "text": "Bake in 200 degree oven for 1 hour, folding / stiring popcorn every 15 mins. Allow to cool slightly but not completely before putting in containers"
     ],
     
]
  ProTips: ["To Pop Popcorn kernels, just place ~ 1/4 cup in brown paper bag in the mircowave. Listen closely to not burn the popcorn.", ""]
  images: ["../CarmelPopcorn.jpg"]
---

