---
title: "Chicken Enchiladas"
date: 2024-03-30T00:00:00-05:00
draft: false
description: "Hearty and filling enchiladas made with shredded chicken, black beans, and green chiles, topped with enchilada sauce and melted cheese."
tags: ['dinner', 'Mexican']
categories: ['Chase']
recipes:
  name: "Chicken Enchiladas"
  author:
    name: "Molly Thompson"
    link: "https://whatmollymade.com/healthy-chicken-enchiladas/"
  prepTime: "20"
  cookTime: "15"
  totalTime: "35"
  yield: "4 servings"
  ingredients: [
    [
      name: "Main Ingredients",
      ingredient: [
        "1½ pounds boneless, skinless chicken breasts (Rotisserie Chicken works great)",
        "1 teaspoon cumin",
        "1 teaspoon garlic powder",
        "Salt and pepper, to taste",
        "1 tablespoon olive oil",
        "1 medium onion, diced",
        "2 cloves garlic, minced",
        "1 (15-ounce) can black beans, drained and rinsed",
        "1 (4-ounce) can diced green chiles",
        "2 cups enchilada sauce (store-bought or homemade)",
        "8 tortillas (regular, gluten-free, or corn)",
        "1½ cups shredded Mexican cheese blend"
      ],
    ],
    [
      name: "Optional Toppings",
      ingredient: [
        "Chopped cilantro",
        "Sliced avocado",
        "Diced tomatoes",
        "Sour cream or plain Greek yogurt",
        "Sliced jalapeños"
      ],
    ],
  ]
  instructions: [
    [
      name: "Cook the Chicken",
      text: "Season the chicken breasts with cumin, garlic powder, salt, and pepper. In a skillet over medium-high heat, heat olive oil and cook the chicken for 6-7 minutes on each side until fully cooked. Remove from heat, let cool, and shred."
    ],
    [
      name: "Prepare the Filling",
      text: "In the same skillet, sauté diced onion and minced garlic until translucent. Add shredded chicken, black beans, and diced green chiles. Stir to combine."
    ],
    [
      name: "Assemble Enchiladas",
      text: "Preheat oven to 350°F (175°C). Warm tortillas to make them pliable. Spread a thin layer of enchilada sauce on the bottom of a 9×13-inch baking dish. Fill each tortilla with the chicken mixture, roll tightly, and place seam-side down in the dish."
    ],
    [
      name: "Top and Bake",
      text: "Pour remaining enchilada sauce over the rolled tortillas and sprinkle with shredded cheese. Bake for 15 minutes until heated through and the cheese is melted and bubbly."
    ],
    [
      name: "Serve",
      text: "Garnish with optional toppings like cilantro, avocado, tomatoes, sour cream, or jalapeños. Serve warm."
    ],
  ]
  ProTips: [
    "For a dairy-free option, use cashew queso instead of shredded cheese.",
    "To make ahead, assemble the enchiladas and refrigerate before baking.",
    "Rotisserie is easy to shread and pre-cooked."
  ]
  images: [""]
---
