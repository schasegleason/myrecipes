---
title: "Pad Thai"
date: 2022-01-09T22:16:44-05:00
draft: false
description: "Easy Pad Thai"
tags: ['dinner','asian','thai']
categories: ['new finds','chase']
recipes: 
  name: "Pad Thai"
  author:
    name: "Tastes Better from Scratch - Lauren Allen"
    link: "https://tastesbetterfromscratch.com/pad-thai/"
  prepTime: "10"
  cookTime: "15"
  totalTime: "35"
  yield: "4"
  ingredients: [
      [
        name: "Main",
        ingredient: [
            "Half bag flat rice noodles",
            "3 Tablespoons oil",
            "3 cloves garlic, minced",
            "2-4 eggs",
            "8 - 16 ounces protien - shrimp, checkien, firm tofu (cut into bite sized pieces)",
            "1 onion, chopped",
            "3 green onions, chopped",
            "1/2 cup dry roasted peanuts",
            "2 limes - 4 tablespoons lime juice",
            "1/2 cup fresh cilantro, roughly chopped",
          ],
      ],
      [
        name: "Sauce",
        ingredient: [
          "2 Tablespoons fish sauce",
          "2 Tablespoons soy sauce",
          "3 Tablespoons light brown sugar",
          "2 Tablespoons rice vinegar",
          "1-2 Tablespoons Sriracha host sauce, to taste",
          "2 Tablespoons creamy peanut butter",
        ],
      ]

  ]
  instructions: [
    [
    name: "Cook noodles ",
    text: "According to package instructions, typically soaking in a large bowl of hot water, just until tender.  Rinse under cold water."
    ],
    [
      "name": "Mix Sauce",
          "text": "Mix the sauce ingredients together. Set aside"
    ],
    [
          "name": "Prep",
          "text": "Heat 1½ tablespoons of oil in a large saucepan or wok over medium-high heat. Mince Garlic and chopp onion, green onion, and cilantro"
     ],
     [ 
          "name": "Cook Protein",
          "text": "Add the shrimp, chicken or tofu, and garlic and onion. The shrimp will cook quickly, about 1-2 minutes on each side, or until pink. If using chicken, cook until just cooked through, about 3-4 minutes, flipping only once."
     ],
     [
          "name": "Add Eggs",
          "text": "Push everything to the side of the pan. Add a little more oil and add the beaten eggs. Scramble the eggs, breaking them into small pieces with a spatula as they cook"
     ],
      [
          "name": "Combine Noodles and Sauce",
          "text": "Add noodles, sauce, bean sprouts and peanuts to the pan (reserving some peanuts for topping at the end). Toss everything to combine.",
      ],
            [
          "name": "Serve",
          "text": "Top with green onions, extra peanuts, cilantro and lime wedges. Serve immediately!",
      ],

]
  ProTips: ["Noodles can take 15 - 25 minutes to cook", "Start cooking protien before other steps (tofu can take awhile to cook)"]
  images: []
---

