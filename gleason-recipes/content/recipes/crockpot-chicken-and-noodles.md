---
title: "Crockpot Chicken and Noodles"
date: 2022-01-10T20:32:12-05:00
draft: false
description: "Perfect Homemade Dinner Rolls Light and fluffy"
tags: ['dinner','american', 'crockpot']
categories: ['new finds','chase']
recipes: 
  name: "Crockpot Chicken and Noodles"
  author:
    name: "Ashlyn Edwards - Belle of the Kitchen"
    link: "https://belleofthekitchen.com/crockpot-chicken-noodles/"
  prepTime: "10"
  cookTime: "420"
  totalTime: "430"
  yield: "6"
  ingredients: [
      [
        name: "Main",
        ingredient: [
            "3 (1-2 pounds) boneless skinless checken breasts",
            "2 cans (or boxes) cream of chicken soup, 10.5 oz each, (don't add water)",
            "1-2 tsp dried parsley",
            "½ tsp seasoned salt",
            "½ tsp garlic powder",
            "½ tsp onion powder",
            "1/2 stick butter, sliced",
            "3 1/2 cups chicken broth",
            "8 oz. egg noodles, uncooked",

          ],
      ]

  ]
  instructions: [
    [
    name: "Prep Crockpot",
    text: "Place chicken breasts in the bottom of the crockpot, then top with the soups (no water added) and sprinkle with the spices. Add butter slices to the top, and pour in the chicken broth."
    ],
    [
      "name": "Cook",
          "text": "Cook over low heat for 5-6 hours, or high heat for 3-4 hours. Remove the chicken from the pot and shred into large chunks with two forks."
    ],
    [
          "name": "Add Noodles",
          "text": "Add the shredded chicken back to the pot along with the dry egg noodles and stir. Continue cooking for 30-45 minutes, until noodles are tender. "
     ],
    
]
  ProTips: ["Cream of Chicken comes in 1 Big 20 oz can", ]
  images: []
---

