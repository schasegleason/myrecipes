---
title: "Beef Fondue"
date: 2022-12-31T01:05:09-05:00
draft: false
description: "New Years Treat"
tags: ['dinner','holiday',]
categories: ['orginals']
recipes: 
  name: "Beef Fondue"
  author:
    name: "Grandma Snow"
  prepTime: "60"
  cookTime: "60"
  totalTime: "120"
  yield: "6-8"
  ingredients: [
      [
        name: "Main",
        ingredient: [
            "2 lbs Beef Round Steak, Cut 3/4 inch thick",
            "1/4 Cup Frozen Orange Juice Concentrate, Defrosted",
            "1/4 Cup Soy Sauce",
            "1/4 Cup Salad Oil",
            "1/4 Cup Water",
            "1 teaspon Lemon Juce",
            "1/4 teaspon Ginger Salad Oil",
            "1 teaspoon Cornstarch",
            "1 Cup Ketchup",
          ],
      ],
      [
        name: "Fondue Batter",
        ingredient: [
          "2 - 1/2 Cups Flour",
          "1/4 Cup Sugar",
          "1 teaspoon Baking Powder",
          "1 teaspoon Garlic Salt",
          "1- 3/4 Cups Milk",
          "2 Eggs, Slightly Beaten",
        ],
      ]

  ]
  instructions: [
    [
    name: "Cut Steak",
    text: "Cut Round Steak into One-Inch Cubes and Place in a Bowl"
    ],
    [
      "name": "Combine Marinade ingredients ",
          "text": "Combine Orange Juice Concentrate, Soy Sauce, Oil, Water, Lemon Juice and Ginger. Pour Over Meat."
    ],
    [
          "name": "Marinate",
          "text": "Cover Bowl and Marinate in Refrigerator 4 - 6 Hours or Overnight, Stirring at Least Once."
     ],
     [ 
          "name": "Remove and Dry Steak",
          "text": "Remove meat from marinade, place on paper towels / pat dry, reserve marinage."
     ],
     [
          "name": "Dipping / Accompanying Sacue",
          "text": "Combine Marinade with Cornstartch in small saucepan: Add Katschup and cook, stirring constantly until thickened."
     ],
      [
          "name": "Foudue Batter",
          "text": "Combine Flour, Sugar, Garlic salt and Baking Power. Combine Milk and Eggs then add to flour mixture, stirring until smoooth ",
      ],
            [
          "name": "Foudue TIME!",
          "text": "Pour Oil in Fondue Pot (NO MORE than half full) and Heat to 375ºF. Spear Beef Cubes, with Fondue Fork Dip in Fondue Batter, If desired and place into hot oil, unitl Godlen Brown (typically 2-3 miuntes). Dont forget the dipping sauce.",
      ],


]
  ProTips: ["Ask the Butcher to cut the steak into 1 inch cubes for you.", "Use plastic table cloth for easy clean up, and crack a window."]
  images: ["../../orginal-recipes/Main_Dishes/OTHER/BEEF%20FONDUE.pdf"]
---

