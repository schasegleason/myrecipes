---
title: "Greek Turkey Burgers"
date: 2024-08-28T00:00:00-05:00
draft: false
description: "Juicy turkey burgers infused with Mediterranean flavors, topped with a creamy homemade tzatziki sauce."
tags: ['dinner', 'Mediterranean']
categories: ['Chase']
recipes:
  name: "Greek Turkey Burgers with Tzatziki Sauce"
  author:
    name: "Heidi / FoodieCrush"
    link: "https://www.foodiecrush.com/greek-turkey-burgers-tzatziki-sauce/"
  prepTime: "15"
  cookTime: "10"
  totalTime: "55"
  yield: "4 burgers"
  ingredients: [
    [
      name: "Turkey Burgers",
      ingredient: [
        "1 pound ground turkey",
        "½ cup fresh spinach leaves, chopped",
        "⅓ cup sun-dried tomatoes, chopped",
        "¼ cup red onion, minced",
        "¼ cup feta cheese, crumbled",
        "2 cloves garlic, pressed or minced",
        "1 egg, whisked",
        "1 tablespoon olive oil",
        "1 teaspoon dried oregano",
        "½ teaspoon kosher salt",
        "½ teaspoon freshly ground black pepper",
        "4 soft whole-wheat hamburger buns",
        "Bibb lettuce leaves",
        "Sliced red onion"
      ],
    ],
    [
      name: "Tzatziki Sauce",
      ingredient: [
        "½ English cucumber, grated (about 1 cup)",
        "1 cup plain Greek yogurt (2%)",
        "1 medium clove garlic, grated or minced (about ½ teaspoon)",
        "1 tablespoon red wine vinegar",
        "1 tablespoon fresh dill, minced",
        "¼ teaspoon kosher salt",
        "⅛ teaspoon freshly ground black pepper"
      ],
    ],
  ]
  instructions: [
    [
      name: "Prepare Tzatziki Sauce",
      text: "Gather the grated cucumber and place in a paper towel; press to remove excess water. In a medium bowl, combine cucumber, Greek yogurt, garlic, red wine vinegar, fresh dill, salt, and pepper. Mix well, cover, and refrigerate for at least 30 minutes or up to 3 days."
    ],
    [
      name: "Prepare Turkey Burger Mixture",
      text: "In a large bowl, add ground turkey, chopped spinach, sun-dried tomatoes, red onion, and feta cheese. In a separate bowl, whisk together the egg, olive oil, oregano, garlic, salt, and pepper. Pour the egg mixture into the turkey mixture and combine using your hands until well mixed."
    ],
    [
      name: "Form and Chill Patties",
      text: "Divide the mixture into four equal portions and shape into patties. Place the patties on a plate, cover, and refrigerate for at least 30 minutes to firm up."
    ],
    [
      name: "Cook Turkey Burgers",
      text: "Heat a non-stick grill pan over medium heat and spray well with cooking spray. Place the turkey burgers on the grill, cover with an upside-down sheet pan or lid, and cook for about 5 minutes per side, or until the internal temperature reaches 165°F."
    ],
    [
      name: "Assemble Burgers",
      text: "Place each turkey burger on a whole-wheat bun and top with Bibb lettuce, sliced red onion, and a generous dollop of tzatziki sauce. Serve immediately."
    ],
  ]
  ProTips: [
    "Refrigerating the patties before cooking helps them hold together better on the grill.",
    "Squeeze out excess water from the grated cucumber to prevent a watery tzatziki sauce."
  ]
  images: [""]
---
