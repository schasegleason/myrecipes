---
title: "24Hour Fruit_Salad"
date: 2022-12-31T01:31:34-05:00
draft: false
description: "Paired with New Years Beef Fondue"
tags: ['side dish','holiday']
categories: ['orginals']
recipes: 
  name: "24Hour Fruit_Salad"
  author:
    name: "Grandma Snow"
  prepTime: "30"
  cookTime: "15"
  totalTime: "180"
  yield: "10"
  ingredients: [
      [
        name: "Main",
        ingredient: [
            "2 Eggs Beaten",
            "4 Tablespoons Vinegar",
            "4 Tablespoons Sugar",
            "2 Tablesppons Butter",
            "1 Cup Whipping Cream, Whipped",
            "1 Can (17 Oz) Fruit Cocktrail, Drained",
            "2 Cans (20 Oz, Each) Pineapple Chunks, Drained",
            "2 Cans (11 Oz, Each) Mandarin Oranges, Drained",
            "3 Bannans, Sliced, Rolled in Lemon Juice",
            "2 Apples, Cut, Rolled in Lemon Juice",
          ],
      ]
    ]

  
  instructions: [
    [
    name: "Create Salad Dressing / Sauce",
    text: "Combine Eggs, Vinegar, and Sugar in a suace pan. Cook Over Medium Heat until Thick, Stirring constantly. Once Thickened Add Butter and let Cool"
    ],
    [
      "name": "Combine",
          "text": "Once cooled, fold mixture into Whipping creaming until combined. Then add all Fruit and combine sauce and fruit ensuring everything is coated"
    ],
    [
          "name": "Stand Overnight",
          "text": "Once mixed together let salad standover night or 24 hours in the fridge."
     ],
    
]
  ProTips: ["Add the Bannas or Apples right before servering, then lemon juice is not needed."]
  images: ["../../orginal-recipes/Salads/SWEET%20SALADS/TWENTY-FOUR%20HOUR%20FRUIT%20SALAD.pdf"]
---

