---
title: "Sesame Chicken"
date: 2022-01-18T20:02:17-05:00
draft: false
description: "Crispy Sesame Chicken with a Sticky Asian Sauce"
tags: ['dinner','asian',]
categories: ['new finds','chase']
recipes: 
  name: "Sesame Chicken"
  author:
    name: "Nicky Corbishley - Kitchen Sancturary"
    link: "https://www.kitchensanctuary.com/crispy-sesame-chicken-sticky-asian-sauce/"
  prepTime: "20"
  cookTime: "15"
  totalTime: "35"
  yield: "4"
  ingredients: [
      [
        name: "Main",
        ingredient: [
            "5 tbsp Vegetable oil",
            "2 eggs - lightly beaten",
            "3 tbsp cornflour (cornstarch)",
            "10 tbsp plain (all-purpose) flour",
            "½ tsp salt",
            "½ tsp pepper",
            "½ tsp garlic salt",
            "2 tsp paprika",
            "3 chicken breast fillets - chopped into bite-size chunks",
          ],
      ],
      [
        name: "Sauce",
        ingredient: [
          "1 tbsp sesame oil - optional",
          "2 cloves garlic - peeled and minced",
          "1 tbsp Chinese rice vinegar or white wine vinegar",
          "2 tbsp honey",
          "2 tbsp sweet chilli sauce / sriracha - to spiciness taste",
          "2 tbsp brown sugar",
          "4 tbsp soy sauce",
        ],
      ],
      [
        name: "Serve with",
        ingredient: [
          "Boiled Rice",
          "2 tbsp sesame seeds",
          "Small bunch spring onions/scallions - chopped",
        ],
      ],

  ]
  instructions: [
    [
    name: "Prep Pan and Start Rice",
    text: "Heat the oil in a wok or large frying pan until very hot. And Start Boiling water / cooking rice."
    ],
    [
      "name": "Prep Ingredients",
          "text": "Whilst the oil is heating, place the egg in one shallow bowl and the cornflour in another shallow bowl. Add the flour, salt, pepper, garlic salt and paprika to another shallow bowl and mix together."
    ],
    [
          "name": "Coat and Cook Chicken",
          "text": "Dredge the chicken in the cornflour, then dip in the egg (make sure all of the chicken is covered in egg wash), and finally dredge it in the seasoned flour. Add to the wok and cook on a high heat for 6-7 minutes, turning two or three times during cooking, until well browned. You may need to cook in two batches (I find I can do it in one batch so long as it's no more than 3 chicken breasts). Remove from the pan and place in a bowl lined with kitchen towels."
     ],
     [ 
          "name": "Mix Sauce Ingredients",
          "text": "Add all of the sauce ingredients to the hot wok, stir and bubble on a high heat until the sauce reduces by about a third (should take 2-3 minutes). Add the chicken back in and toss in the sauce to coat. Cook for 1-2 minutes."
     ],
     [
          "name": "Serve",
          "text": "Turn off the heat and divide between four bowls. Serve with boiled rice and top with sesame seeds and spring onions."
     ],
]
  ProTips: [""]
  images: []
---

